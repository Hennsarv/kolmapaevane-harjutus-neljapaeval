﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KolmapäevaneHarjutusNeljapäeval
{
    class Program
    {
        static void Main(string[] args)
        {
            // pidin lugema sisse õpilaste nimed
            List<string> nimed = new List<string>();
            //string[] nimed = new string[100];
            //int i = 0;
            while (true)
            {
                Console.Write("Anna nimi: ");
                string nimi = Console.ReadLine();
                if (nimi == "") break;
                nimed.Add(nimi);
                //nimed[i++] = nimi;
            }
            List<int> vanused = new List<int>();
            foreach(string nimi in nimed)
            {
                Console.Write($"Anna õpilase {nimi} vanus: ");
                vanused.Add(int.Parse(Console.ReadLine()));
            }
            // trükime välja
            for (int i = 0; i < nimed.Count; i++)
            {
                Console.WriteLine($"Õpilane {nimed[i] } vanusega {vanused[i]}");
            }
            double keskmine = 0;
            foreach (int v in vanused) keskmine += v;
            keskmine /= nimed.Count;
            List<int> parimad = new List<int>();
            int kes = 0; // see on esimese venna number!!!
            //double kaugus = keskmine - vanused[0];
            //if (kaugus < 0) kaugus = -kaugus;
            double kaugus = Math.Abs(keskmine - vanused[0]);

            for (int i = 1; i < nimed.Count; i++)
            {
                double uuskaugus = Math.Abs(keskmine - vanused[i]);
                if (uuskaugus == kaugus) parimad.Add(i);

                if (uuskaugus < kaugus)
                {
                    parimad = new List<int>();
                    kaugus = uuskaugus;
                    kes = i;
                }
            }
            Console.WriteLine($"Keskmisele {keskmine} on kõige lähemal {nimed[kes]}");

            if (parimad.Count > 0)
            {
                Console.WriteLine("temale lisaks:");
                foreach (int i in parimad)
                    Console.WriteLine($"\t{nimed[i]}");  
            }



        }
    }
}
